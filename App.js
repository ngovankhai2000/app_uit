import React from 'react';
import {
  Text,
  TouchableOpacity,
  NativeModules,
  NativeEventEmitter,
  Platform,
  DeviceEventEmitter,
} from 'react-native';
import RNMomosdk from 'react-native-momosdk';
import axios from 'axios';
import {MOMO_CONFIG} from './config';
import {requestPayment} from './api/momo';
const RNMomosdkModule = NativeModules.RNMomosdk;
const EventEmitter = new NativeEventEmitter(RNMomosdkModule);

const onPress = async () => {
  console.log('Handle onPress');
  let jsonData = {};
  jsonData.enviroment = MOMO_CONFIG.enviroment; //SANBOX OR PRODUCTION
  jsonData.action = MOMO_CONFIG.action; //DO NOT EDIT
  jsonData.partner = MOMO_CONFIG.partner;
  jsonData.merchantname = MOMO_CONFIG.merchantname; //edit your merchantname here
  jsonData.merchantcode = MOMO_CONFIG.merchantcode; //edit your merchantcode here
  jsonData.merchantnamelabel = MOMO_CONFIG.merchantNameLabel;
  jsonData.description = MOMO_CONFIG.billdescription;
  jsonData.amount = MOMO_CONFIG.amount; //order total amount
  jsonData.appScheme = MOMO_CONFIG.appScheme; // iOS App Only , match with Schemes Indentify from your  Info.plist > key URL types > URL Schemes

  if (Platform.OS === 'android') {
    let dataPayment = await RNMomosdk.requestPayment(jsonData);
    momoHandleResponse(dataPayment);
  } else {
    RNMomosdk.requestPayment(jsonData);
  }
};

const momoHandleResponse = async response => {
  try {
    console.log('App momo res');
    console.log(JSON.stringify(response, null, 2));
    if (response && response.status == 0) {
      console.log('run 1');
      //SUCCESS continue to submit momoToken,phonenumber to server
      let appData = response.data;
      let customerNumber = response.phonenumber;

      const paymentResponseFromServer = await requestPayment(
        appData,
        MOMO_CONFIG.amount,
        customerNumber,
        response.orderId,
      );
      console.log(JSON.stringify(paymentResponseFromServer), null, 2);
    } else {
      console.log('run 2');
      //let message = response.message;
      //Has Error: show message here
    }
  } catch (ex) {
    console.log(ex);
  }
};

export default class App extends React.Component {
  componentDidMount() {
    EventEmitter.addListener(
      'RCTMoMoNoficationCenterRequestTokenReceived',
      response => {
        try {
          console.log('<MoMoPay>Listen.Event::' + JSON.stringify(response));
          if (response && response.status == 0) {
            //SUCCESS: continue to submit momoToken,phonenumber to server
            let fromapp = response.fromapp; //ALWAYS:: fromapp==momotransfer
            let momoToken = response.data;
            let phonenumber = response.phonenumber;
            let message = response.message;
            let orderId = response.refOrderId;
          } else {
            //let message = response.message;
            //Has Error: show message here
          }
        } catch (ex) {
          console.log('Event ex ' + ex);
        }
      },
    );
    //OPTIONAL
    EventEmitter.addListener(
      'RCTMoMoNoficationCenterRequestTokenState',
      response => {
        console.log('<MoMoPay>Listen.RequestTokenState:: ' + response.status);
        // status = 1: Parameters valid & ready to open MoMo app.
        // status = 2: canOpenURL failed for URL MoMo app
        // status = 3: Parameters invalid
      },
    );
  }
  render() {
    return (
      <TouchableOpacity onPress={onPress}>
        <Text> Me</Text>
      </TouchableOpacity>
    );
  }
}
