const {postFetch} = require('./../utils/fetch');
const {BACKEND_HOST} = require('./../config');

module.exports.requestPayment = (
  momoAppResponse,
  amount,
  customerNumber,
  orderId,
) => {
  const payloadRequest = {
    appData: momoAppResponse,
    amount,
    customerNumber,
    orderId,
  };
  console.log('POST  -> ' + `${BACKEND_HOST}/forwardPayment`);
  console.log(JSON.stringify(payloadRequest, null, 2));
  return postFetch(`${BACKEND_HOST}/forwardPayment`, payloadRequest);
};
