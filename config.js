module.exports = {
  MOMO_CONFIG: {
    merchantname: 'UIT',
    merchantcode: 'MOMOPXBE20210507',
    merchantNameLabel: 'UIT',
    billdescription: 'Fast and Furious 8',
    amount: 50000,
    enviroment: '0', //"0": SANBOX , "1": PRODUCTION
    action: 'gettoken',
    partner: 'merchant',
    appScheme: 'momopxbe20210507',
  },

  BACKEND_HOST: 'http://10.0.0.188:3000',
};
