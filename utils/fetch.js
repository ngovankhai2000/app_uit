import axios from 'axios';

module.exports = {
  getFetch: url => {
    return axios
      .get(url)
      .then(res => res.data)
      .catch(ex => {
        return {
          error: true,
          message: ex.message || 'Unknown error',
        };
      });
  },

  postFetch: (url, data) => {
    return axios
      .post(url, data)
      .then(res => res.data)
      .catch(ex => ({
        error: true,
        message: ex.message || 'unknown error',
      }));
  },
};
